import shutil
import json
import os
import unittest
import logging
from pathlib import Path
from datadirtest import DataDirTester, TestDataDir
from keboola.component import CommonInterface


class PgpEncryptionTest(TestDataDir):
    def run_component(self):
        config_dict = self.ci.configuration.config_data
        super().run_component()
        if config_dict['parameters']["direction"] == "encrypt":
            # Due to encryption not producing the same output file, to test encryption we need to decrypt as well
            logging.info("Encrypted, now decrypting")
            self.move_encrypted_file()
            self.override_config()
            super().run_component()

    def move_encrypted_file(self):
        source_dir = os.path.join(self.data_dir, "source", "data", "out", "files")
        target_dir = os.path.join(self.data_dir, "source", "data", "in", "files")
        file_names = os.listdir(source_dir)
        for file_name in file_names:
            shutil.move(os.path.join(source_dir, file_name), target_dir)

    def setUp(self):
        super().setUp()
        source_out_files = os.path.join(self.data_dir, "source", "data", "out", "files")
        expected_out_files = os.path.join(self.data_dir, "expected", "data", "out", "files")
        source_out_tables = os.path.join(self.data_dir, "source", "data", "out", "tables")
        source_in_files = os.path.join(self.data_dir, "source", "data", "in", "files")
        Path(source_out_files).mkdir(parents=True, exist_ok=True)
        Path(source_in_files).mkdir(parents=True, exist_ok=True)
        Path(source_out_tables).mkdir(parents=True, exist_ok=True)
        Path(expected_out_files).mkdir(parents=True, exist_ok=True)
        self.ci = CommonInterface(data_folder_path=os.path.join(self.data_dir, "source", "data"))

    def override_config(self):
        config_path = os.path.join(self.data_dir, "source", "data")
        with open(os.path.join(config_path, 'config.json'), 'r') as config_file:
            config_data = json.load(config_file)
            config_data['parameters']["direction"] = "decrypt"
        with open(os.path.join(config_path, 'config.json'), 'w') as config_file_out:
            json.dump(config_data, config_file_out)


class TestComponent(unittest.TestCase):

    def test_functional(self):
        functional_tests = DataDirTester(test_data_dir_class=PgpEncryptionTest)
        functional_tests.run()


if __name__ == "__main__":
    unittest.main()

PGP Encrypt Processor for CSAS
=============

This processor enables you to encrypt and decrypt files using PGP.

**Table of contents:**

[TOC]

Prerequisites
=============

You need a PGP private key, public key, and a passphrase.

Configuration
=============

#secret_key
-------
Required (for decrypt) : A PGP secret key (with newlines as the '\n' character)

#public_key
-------
Required (for encrypt)   : A PGP public key (with newlines as the '\n' character)

#passphrase
-------
Required  (for decrypt) : PGP passphrase

direction
-------
Required : direction of encryption 'encrypt' or 'decrypt'

source
-------
Optional : source of file to encrypt, either 'tables' or 'files'. Default for 'encrypt' direction is 'tables'. Default
for 'decrypt' direction is 'files'.

destination
-------
Optional : destination of encrypted/decrypted files, either 'tables' or 'files'. Default for 'encrypt' direction is '
files'. Default for 'decrypt' direction is 'tables'.

name
-------
Optional : name of file to encrypt. Default None : all files encrypted/decrypted

pattern
-------
Optional : regex pattern of file to encrypt. Default None : all files encrypted/decrypted

skip-empty-files
-------
Optional : If set to true, empty files will be skipped from processing. Otherwise, the processing will fail

Sample Configurations
=============
Encryption
```json
"parameters": {
"#public_key": "-----BEGIN PGP PUBLIC KEY BLOCK-----\nVersion: Keybase OpenPGP v1.0.0\nComment: https://keybase.io/crypto\n\nxxx\nxxxx\nxxx\nxxxxx\n-----END PGP PUBLIC KEY BLOCK-----\n",
"direction": "encrypt",
"skip_empty_files": false
}
```

Decryption
```json
"parameters": {
"#secret_key": "-----BEGIN PGP PRIVATE KEY BLOCK-----\nVersion: Keybase OpenPGP v1.0.0\nComment: https://keybase.io/crypto\n\nxxxxx\nxxxxxxx\nxxxxxxx\n-----END PGP PRIVATE KEY BLOCK-----\n",
"#passphrase": "passphrase",
"direction": "decrypt",
"skip_empty_files": true
}
```

Use as processor:
```json
{
  "processors": {
    "after": [
      {
        "definition": {
          "component": "kds-team.processor-pgp-encryption-csas"
        },
        "parameters": {
          "#secret_key": "key",
          "#passphrase": "passphrase",
          "direction": "decrypt"
        }
      },
      {
        "definition": {
          "component": "keboola.processor-create-manifest"
        },
        "parameters": {
          "columns_from": "header"
        }
      },
       {
            "definition": {
                "component": "keboola.processor-skip-lines"
            },
            "parameters": {
                "lines": 1
            }
        }
    ]
  }
}
```

Development
=============

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path in
the `docker-compose.yml` file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clone this repository, init the workspace and run the component with following command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone https://bitbucket.org/kds_consulting_team/kds-team.processor-pgp-encryption-csas/src/master/ kds-team.processor-pgp-encryption-csas
cd kds-team.processor-pgp-encryption-csas
docker-compose build
docker-compose run --rm dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run the test suite and lint check using this command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose run --rm test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Integration
===========

For information about deployment and integration with KBC, please refer to the
[deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/)

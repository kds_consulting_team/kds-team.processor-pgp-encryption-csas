import logging
import os
import re
from typing import List

from keboola.component.base import ComponentBase
from keboola.component.dao import FileDefinition
from keboola.component.exceptions import UserException

from crypto.pgp import PGPEncryptor, PGPEncryptorException

KEY_SKIP_EMPTY_FILES = 'skip_empty_files'

KEY_ENCRYPT_NAME = "name"
KEY_ENCRYPT_PATTERN = "pattern"

KEY_SOURCE = "source"
KEY_DESTINATION = "destination"

KEY_SECRET_KEY = "#secret_key"
KEY_PUBLIC_KEY = "#public_key"
KEY_PASSPHRASE = "#passphrase"
KEY_ENCRYPT_DIRECTION = "direction"

ENCRYPT_REQUIRED = [KEY_PUBLIC_KEY]
DECRYPT_REQUIRED = [KEY_SECRET_KEY, KEY_PASSPHRASE]
REQUIRED_PARAMETERS = [[DECRYPT_REQUIRED, ENCRYPT_REQUIRED], KEY_ENCRYPT_DIRECTION]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    def __init__(self):
        super().__init__()
        self.default_source = "files"
        self.default_destination = "tables"

    def run(self):
        self.validate_configuration_parameters(REQUIRED_PARAMETERS)
        self.validate_image_parameters(REQUIRED_IMAGE_PARS)
        params = self.configuration.parameters

        if "\n" not in params.get(KEY_SECRET_KEY):
            raise UserException("'\\n' character not found in secret key, "
                                "make sure the secret key you input is correct. "
                                "Key should look like e.g. : -----BEGIN PGP PRIVATE KEY BLOCK-----"
                                "\\nVersion: Keybase OpenPGP v1.0.0\\"
                                "nComment: https://keybase.io/crypto\\n"
                                "\\nKEYLINE\\n")

        encrypt_direction = params.get(KEY_ENCRYPT_DIRECTION)
        if encrypt_direction == "encrypt":
            self.default_source = "tables"
            self.default_destination = "files"

        destination = params.get(KEY_DESTINATION, self.default_destination)

        in_files = self.get_input_files()
        encryptor = self.get_encryptor()
        for in_file in in_files:
            logging.info(f"{encrypt_direction}ing {in_file.name} to {destination}")

            file_is_empty = os.stat(in_file.full_path).st_size == 0
            if file_is_empty:
                if params.get(KEY_SKIP_EMPTY_FILES, False):
                    logging.warning(f'File {in_file.name} is empty! Skipping.')
                    continue
                else:
                    raise UserException(f'File {in_file.name} is empty! '
                                        'Please set the "skip_empty_files" parameter to true to skip empty files.')

            out_file = self.get_output_definition(in_file)
            try:
                self.encrypt_file(encryptor, in_file, out_file)
            except PGPEncryptorException as pgp_err:
                raise UserException(pgp_err)from pgp_err

    def get_input_files(self) -> List[FileDefinition]:
        params = self.configuration.parameters
        source = params.get(KEY_SOURCE, self.default_source)
        if source == 'files':
            in_files = self.get_input_files_definitions(only_latest_files=False)
        elif source == 'tables':
            in_files = self.get_input_tables_definitions()
        else:
            raise UserException(
                f"Encryption source '{source}' is not supported. Only 'files' or 'tables'")
        filtered_files = self.filter_files(in_files)
        return filtered_files

    def filter_files(self, in_files):
        params = self.configuration.parameters
        name = params.get(KEY_ENCRYPT_NAME)
        pattern = params.get(KEY_ENCRYPT_PATTERN)
        if name:
            filtered_files = self.get_input_files_by_name(in_files, name)
        elif pattern:
            filtered_files = self.get_input_files_by_pattern(in_files, pattern)
        else:
            filtered_files = in_files

        return filtered_files

    @staticmethod
    def get_input_files_by_name(in_files, name):
        filtered_files = []
        for file in in_files:
            if file.name == name:
                filtered_files.append(file)
        return filtered_files

    @staticmethod
    def get_input_files_by_pattern(in_files, pattern):
        filtered_files = []
        for file in in_files:
            matches = re.findall(pattern, file.name)
            if matches:
                filtered_files.append(file)
        return filtered_files

    def get_encryptor(self):
        params = self.configuration.parameters

        secret_key = params.get(KEY_SECRET_KEY)
        public_key = params.get(KEY_PUBLIC_KEY)
        passphrase = params.get(KEY_PASSPHRASE)

        return PGPEncryptor(secret_key=secret_key, public_key=public_key, passphrase=passphrase)

    def encrypt_file(self, encryptor, in_file, out_file):
        params = self.configuration.parameters
        source = params.get(KEY_SOURCE, self.default_source)
        encrypt_direction = params.get(KEY_ENCRYPT_DIRECTION)

        if encrypt_direction == "encrypt":
            encryptor.encrypt_file(in_file, out_file)
        elif encrypt_direction == "decrypt":
            encryptor.decrypt_file(in_file, out_file)
        else:
            raise UserException(f"Cannot perform {encrypt_direction} on {source}, please make sure "
                                f"direction:{encrypt_direction} and source:{source} are valid")

    def get_output_definition(self, in_file):
        params = self.configuration.parameters
        destination = params.get(KEY_DESTINATION, self.default_destination)
        if destination == "tables":
            out_file = self.create_out_table_definition(in_file.name)
        elif destination == "files":
            out_file = self.create_out_file_definition(in_file.name)
        else:
            raise UserException(f"Destination '{destination}' does not exist, use 'tables' or 'files'")
        return out_file


if __name__ == "__main__":
    try:
        comp = Component()
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)

import warnings

import pgpy
from cryptography.utils import CryptographyDeprecationWarning
from pgpy.constants import CompressionAlgorithm

from crypto.encryptor import Encryptor

# suppress crypto warnings
warnings.filterwarnings("ignore", category=CryptographyDeprecationWarning)


class PGPEncryptorException(Exception):
    pass


class PGPEncryptor(Encryptor):
    def __init__(self, secret_key=None, public_key=None, passphrase=None):
        self.secret_key = secret_key
        self.public_key = public_key
        self.passphrase = passphrase

    def decrypt_file(self, input_file, output_file):
        with open(input_file.full_path, "rb") as in_file:
            data_to_decrypt = in_file.read()

        pgp_file = pgpy.PGPMessage.from_blob(data_to_decrypt)

        try:
            key, _ = pgpy.PGPKey.from_blob(self.secret_key)
        except ValueError as val_err:
            raise PGPEncryptorException(
                "Invalid Secret Key, make sure you use '\\n' character for new line") from val_err
        with key.unlock(self.passphrase):
            decrypted_data = key.decrypt(pgp_file).message

        if isinstance(decrypted_data, str):
            # decrypted_data = key.decrypt(pgp_file).message sometimes returns str sometimes bytearray
            # depending on input file
            decrypted_data = decrypted_data.encode()

        with open(output_file.full_path, "wb") as out_file:
            out_file.write(decrypted_data)

    def encrypt_file(self, input_file, output_file):
        with open(input_file.full_path, "rb") as in_file:
            data_to_encrypt = in_file.read()
        public_key, _ = pgpy.PGPKey.from_blob(self.public_key)
        message = pgpy.PGPMessage.new(data_to_encrypt, compression=CompressionAlgorithm.Uncompressed)
        enc_message = public_key.pubkey.encrypt(message)
        encrypted_data = bytes(enc_message)
        with open(output_file.full_path, "wb") as out_file:
            out_file.write(encrypted_data)

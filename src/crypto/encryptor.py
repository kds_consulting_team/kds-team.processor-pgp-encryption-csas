import abc


class Encryptor(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def decrypt_file(self, input_file, output_file):
        pass

    @abc.abstractmethod
    def encrypt_file(self, input_file, output_file):
        pass
